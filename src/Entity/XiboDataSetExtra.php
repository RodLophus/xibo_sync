<?php

/*
 * Criar um link simbolico deste arquivo em:
 * .../vendor/xibosignage/oauth2-xibo-cms/src/Entity
 */

namespace Xibo\OAuth2\Client\Entity;

use Xibo\OAuth2\Client\Exception\XiboApiException;

class XiboDataSetExtra extends XiboDataSet
{

    public function importJson($json)
    {
        $jsonArray = is_array($json) ? $json : json_decode($json);

        $response = $this->doPost('/dataset/importjson/' . $this->dataSetId, array('json' => $jsonArray));

        return true;
    }

    public function getData()
    {
        $response = $this->doGet('/dataset/data/'. $this->dataSetId);

        return $response;
    }
    

    public function deleteRowById($rowId)
    {
        $this->doDelete('/dataset/data/' . $this->dataSetId . '/' . $rowId);
        
        return true;
    }

}
